# Soccer-Extractor #

##### [University Politecnica delle Marche](www.univpm.it)
#####Department of Information Engineering - Knowledge Discovery & Management Group - Prof. Claudia Diamantini (c.diamantini@univpm.it) 

Soccer-Extractor is a custom DBpedia extractor and a python script. It extracts data from the Template ["CarrieraSportivo"](https://it.wikipedia.org/wiki/Template:Carriera_sportivo) used in the Italian Wikipedia and compose a RDF graph wich can be serialized in turtle, nt etc.  
The dataset (as far as Nov '15) is a large collection of soccer players' careers statistics.
The dataset (22_11_2015) can be retrieved at [soccer_2015_11_22.ttl](https://bitbucket.org/tsiteam/soccer-extractor/downloads/soccer_2015_11_22.ttl), while the Log file at [soccer-extractor_2015_11_22.log](https://bitbucket.org/tsiteam/soccer-extractor/downloads/soccer-extractor_2015_11_22.log).
Basically it is intended to use as a custom extractor.
Everyone can contributes at this project.

ITA - [Prezi Presentation](http://prezi.com/qnlnux4sh4le/?utm_campaign=share&utm_medium=copy&rc=ex0share)
* * *
### Initial Contributors  ###

* Federico Fioravanti 
* Marco Mariani
* Simone Papalini

### Phyton libraries needed ###

* RDFlib v 4.2.1

### NEWS - 12/12/2015 ###
Our dataset was published by the dpbedia.it project. [Here the link to the news.](http://it.dbpedia.org/2015/12/the-soccer-extractor/)